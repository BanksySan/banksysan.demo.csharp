﻿namespace BanksySan.Demo.CSharp.Version7
{
    using System;

    internal abstract class Animal { }

    internal class Dog : Animal
    {
        public void Bark()
        {
            Console.WriteLine("Woof woof");
        }
    }

    internal class Cat : Animal
    {
        public void Meow()
        {
            Console.WriteLine("Meow");
        }
    }

    internal class Canary : Animal
    {
        public void Speak()
        {
            Console.WriteLine("I Taw a Putty Tat");
        }
    }
}