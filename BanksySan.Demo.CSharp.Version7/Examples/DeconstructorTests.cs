﻿namespace BanksySan.Demo.CSharp.Version7.Examples
{
    using System;
    using NUnit.Framework;

    [TestFixture]
    public class DeconstructorTests
    {
        [Test]
        public void Example()
        {
            var (firstName1, lastName1) = new Person("David", "Banks");
            var (firstName2, lastName2) = new Person("Batman", "Suparman");

            Console.WriteLine($"First: {firstName1} {lastName1}. Second: {firstName2} {lastName2}");
        }
    }

    public class Person
    {
        public Person(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        public string FirstName { get; }

        public string LastName { get; }

        public void Deconstruct(out string firstName, out string lastName)
        {
            firstName = FirstName;
            lastName = LastName;
        }

        public void Separate(out string firstName, out string lastName)
        {
            firstName = FirstName;
            lastName = LastName;
        }
    }
}