﻿namespace BanksySan.Demo.CSharp.Version7
{
    using System;
    using NUnit.Framework;

    [TestFixture]
    public class ImplicitOutParametersTests
    {
        [Test]
        public void Example()
        {
            var myNumericString = "1234";

            if (int.TryParse(myNumericString, out var numeric))
                Console.WriteLine(
                    $"I didn't have to declare the variable of the out parameter before I used it!  It parsed to {numeric}.");
        }
    }
}