﻿namespace BanksySan.Demo.CSharp.Version7.Examples
{
    using System;
    using NUnit.Framework;

    [TestFixture]
    public class LiteraChangesTests
    {
        [Test]
        public void Example()
        { 
            var i = 1_000_000;
            var h = 0x0F_42_40;
            var b = 0b1111_0100_0010_0100_0000;

            Console.WriteLine($"(i = {i}, h = {h}, b = {b})");
        }
    }
}
