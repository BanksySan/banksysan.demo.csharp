﻿namespace BanksySan.Demo.CSharp.Version7.Examples
{
    using System;
    using System.Runtime.CompilerServices;
    using NUnit.Framework;

    [TestFixture]
    public class ThrowExpressions
    {
        [Test]
        [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
        public void Example()
        {
            var a = true;
            var b = false;

            try
            {
                var c = a ? throw new Exception("Expected a to be false") : a;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            try
            {
                var d = b ? b : throw new Exception("Expected b to be true.");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}