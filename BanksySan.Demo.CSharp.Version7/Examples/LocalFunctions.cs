﻿namespace BanksySan.Demo.CSharp.Version7.Examples
{
    using System;
    using NUnit.Framework;

    [TestFixture]
    public class LocalFunctions
    {
        [Test]
        public void Example()
        {

            var now = GetNow();

            DateTime GetNow()
            {
                return DateTime.Now;
            }

            Console.WriteLine($"Now: {now}");
        }
    }
}