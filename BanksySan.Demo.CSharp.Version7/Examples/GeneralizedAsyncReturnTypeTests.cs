﻿namespace BanksySan.Demo.CSharp.Version7.Examples
{
    using System;
    using System.Threading.Tasks;
    using NUnit.Framework;

    [TestFixture]
    public class GeneralizedAsyncReturnTypeTests
    {
        public async ValueTask<int> Foo()
        {
            var task = new ValueTask<int>(100);

            return await task;
        }

        [Test]
        public async Task Example()
        {
            Console.WriteLine("Before method");
            var result = await Foo();
            Console.WriteLine($"Finished.  Result was {result}");
        }
    }
}