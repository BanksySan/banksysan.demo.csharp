﻿namespace BanksySan.Demo.CSharp.Version7.Examples
{
    using System;
    using NUnit.Framework;

    [TestFixture]
    public class TuplesExample
    {
        public (int squared, int doubled, double sqrRoot) DoMaths(int i)
        {
            var squared = i * i;
            var doubled = i * 2;
            var sqrRoot = Math.Pow(i, 0.5);
            
            return (squared, doubled, sqrRoot);
        }

        public (int, int, double) DoAnonymousMaths(int i)
        {
            return DoMaths(i);
        }

        [Test]
        public void Example()
        {
            Console.WriteLine("To use Tules we need to reference the System.ValueTuple Nuget package.");

            const int NUMBER = 10;
            var maths = DoMaths(NUMBER);
            var anonymousMaths = DoAnonymousMaths(NUMBER);

            Console.WriteLine($"If we do anomymous maths with {NUMBER} we get {nameof(anonymousMaths.Item1)}: {anonymousMaths.Item1}, {nameof(anonymousMaths.Item2)}: {anonymousMaths.Item2}, {nameof(anonymousMaths.Item3)}: {anonymousMaths.Item3}.");
            Console.WriteLine("Not too helpful, but we can name the values.");
            Console.WriteLine($"If we do maths with {NUMBER} we get {nameof(maths.sqrRoot)}: {maths.sqrRoot}, {nameof(maths.doubled)}: {maths.doubled}, {nameof(maths.squared)}: {maths.squared}.");

            var sistineChapelCoordinates = (north: 41.9029, east: 12.4545);
            Console.WriteLine($"We can create implicit local tupules as well.  The Sistine Chapel is at {sistineChapelCoordinates.Item1}° N, {sistineChapelCoordinates.Item2}° E.");
            Console.WriteLine($"We can create implicit named local tupules as well.  The Sistine Chapel is at {sistineChapelCoordinates.north}° N, {sistineChapelCoordinates.east}° E.");
        }
    }
}