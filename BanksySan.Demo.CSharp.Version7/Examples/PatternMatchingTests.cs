﻿namespace BanksySan.Demo.CSharp.Version7.Examples
{
    using System;
    using NUnit.Framework;

    [TestFixture]
    public class PatternMatchingTests
    {
        [Test]
        public void Example()
        {
            var o = (object) new DateTime();
            var o2 = new object();

            Console.WriteLine("I can test and assign in one statement.");
            Console.WriteLine("d is a DateTime, but it's been boxed into an object o");

            if (o is DateTime)
            {
                Console.WriteLine("o is indeed a DateTime.");
            }

            if (!(o2 is DateTime))
            {
                Console.WriteLine("o2 is not a DateTime.");
            }

            if (o is DateTime dateTime)
            {
                Console.WriteLine($"The value of the dateTime is {dateTime}");
            }

            var canary = (Animal) new Canary();

            switch (canary)
            {
                case Dog d:
                    d.Bark();
                    break;
                case Cat c:
                    c.Meow();
                    break;
                case Canary c:
                    c.Speak();
                    break;
            }

        }
    }
}