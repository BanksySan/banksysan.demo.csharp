﻿namespace BanksySan.Demo.CSharp.Version7.Examples
{
    using System;
    using NUnit.Framework;

    [TestFixture]
    public class ReturningByRefTests
    {
        private static void TestByValue()
        {
            Console.WriteLine("Testing by value");
            var i1 = 0;

            Console.WriteLine($"{i1}");
            var i2 = PlusOneValue(ref i1);

            Console.WriteLine($"{i1}");
            i2++;
            Console.WriteLine($"{i1}");
        }

        private static void TestByReference()
        {
            Console.WriteLine("Testing by reference");
            var i1 = 0;

            Console.WriteLine($"{i1}");
            ref int i2 = ref PlusOneReference(ref i1);

            Console.WriteLine($"{i1}");
            i2++;
            Console.WriteLine($"{i1}");
        }

        private static ref int PlusOneReference(ref int i)
        {
            i++;
            return ref i;
        }

        private static int PlusOneValue(ref int i)
        {
            i++;
            return i;
        }

        [Test]
        public void Example()
        {
            TestByValue();
            TestByReference();
        }
    }
}