namespace BanksySan.Demo.CSharp.Version6
{
    using System;

    [AttributeUsage(AttributeTargets.All)]
    public sealed class MessageAttribute : Attribute
    {
        public string Message { get; set; }
    }
}