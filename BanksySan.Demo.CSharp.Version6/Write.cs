namespace BanksySan.Demo.CSharp.Version6
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal static class Write
    {
        public static void ToConsole<TKey, TValue>(IEnumerable<KeyValuePair<TKey, TValue>> examples)
        {
            var builder = new StringBuilder();

            foreach (var example in examples)
            {
                builder.AppendFormat("{0,30}: {1}", example.Key, example.Value);
                builder.AppendLine();
            }

            Console.Write(builder);
        }
    }
}