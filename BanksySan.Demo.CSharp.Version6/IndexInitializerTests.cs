﻿namespace BanksySan.Demo.CSharp.Version6
{
    using System.Collections.Generic;

    public class IndexInitializerTests
    {
        internal class IndexerClass<TKey, TValue>
        {
            private readonly Dictionary<TKey, TValue> _contents = new Dictionary<TKey, TValue>();

            public TValue this[TKey index]
            {
                get { return _contents[index]; }
                set { _contents.Add(index, value); }
            }
        }
    }
}