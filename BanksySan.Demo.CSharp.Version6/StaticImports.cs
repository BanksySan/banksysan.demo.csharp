﻿using static System.Console;
using static BanksySan.Demo.CSharp.Version6.StaticFoo;

namespace BanksySan.Demo.CSharp.Version6
{
    using NUnit.Framework;

    [TestFixture]
    public class StaticImports
    {
        [Test]
        public void Example()
        {
            WriteLine("This is a static import of the Console type.");
            WriteLine(Message);
            WriteLine(BufferHeight);
        }
    }
}