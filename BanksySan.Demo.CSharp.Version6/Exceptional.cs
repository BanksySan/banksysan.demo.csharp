namespace BanksySan.Demo.CSharp.Version6
{
    using System;

    internal abstract class Exceptional
    {
        public void ThrowException()
        {
            try
            {
                throw new Exception("Something");
            }
            catch (Exception) when (ExceptionPredicate())
            {
                Console.WriteLine("Exception was filtered.");
            }
            catch (Exception)
            {
                Console.WriteLine("Exception not filtered.");
            }
        }

        public abstract bool ExceptionPredicate();
    }
}