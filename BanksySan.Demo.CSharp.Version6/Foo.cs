﻿namespace BanksySan.Demo.CSharp.Version6
{
    public class Foo
    {
        public float this[object index]
        {
            get { return 123.456f; }
            set { }
        }

        public void FooMethod(string fooParameter)
        {
        }

        public T Calculate<T>() where T : new()
        {
            return new T();
        }

        public class InnerFoo
        {
            public string InnerFooProperty { get; set; }

            public int Number { get; set; }

            public void InnerFooMethod(short innerFooMethodParameter)
            {
            }
        }
    }
}