namespace BanksySan.Demo.CSharp.Version6
{
    internal static class StaticMemberInitialisation
    {
        public static string ReadOnlyProperty { get; } = "This is a static read-only property.";
        public static string ReadWriteProperty { get; set; } = "This is a static read and write property.";
    }
}