﻿namespace BanksySan.Demo.CSharp.Version6.Examples
{
    using System.Collections.Generic;
    using NUnit.Framework;

    [TestFixture]
    public class IndexInitializerTests
    {
        [Test]
        public void Examples()
        {
            var foo = new Dictionary<string, string> {{"hello", "world"}, {"goodbye", "cruel world"}};

            var bar = new Dictionary<string, string> {["hello"] = "world", ["goodbye"] = "cruel world"};

            var myClass = new Version6.IndexInitializerTests.IndexerClass<int, string>
            {
                [4] = "Four",
                [7] = "Seven",
                [-2] = "Minus two",
                [int.MaxValue] = "A big number"
            };
        }
    }
}