﻿namespace BanksySan.Demo.CSharp.Version6.Examples
{
    using System.Collections.Generic;
    using NUnit.Framework;

    [TestFixture]
    public class ExtensionAddTests
    {
        [Test]
        public void Examples()
        {
            var collection = new List<int>
            {
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
                0
            };

            var fooCollection = new FooCollection
            {
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
                0
            };
        }
    }
}