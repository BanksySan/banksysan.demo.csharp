namespace BanksySan.Demo.CSharp.Version6.Examples
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using NUnit.Framework;

    [TestFixture]
    [Message(Message = "This is the " + nameof(MessageAttribute) + "attribute.")]
    public class NameOfTests
    {
        [TestCase("dummy")]
        public void Examples(string myStringParameter = null, params int[] paramArray)
        {
            var foo = new Foo();
            var innerFoo = new Foo.InnerFoo();

            var memberInfo = typeof(NameOfTests);
            var messageAttributeValue =
                (string)
                memberInfo.CustomAttributes.Single(x => x.AttributeType == typeof(MessageAttribute))
                    .NamedArguments?.Single()
                    .TypedValue.Value;

            var examples = new Dictionary<string, string>
            {
                {"This method", nameof(Examples)},
                {"This class type", nameof(NameOfTests)},
                {"This method's attribute", nameof(TestAttribute)},
                {"This class's attribute", nameof(TestFixtureAttribute)},
                {"This method's parameter", nameof(myStringParameter)},
                {"This method's param array", nameof(paramArray)},
                {"Another class type", nameof(Foo)},
                {"Local variable", nameof(foo)},
                {"Static class type", nameof(StaticFoo)},
                {"Inner class", nameof(innerFoo)},
                {
                    "Inner class property",
                    nameof(innerFoo.InnerFooProperty)
                },
                {"Inner class method", nameof(foo.FooMethod)},
                {"Value from attribute property", messageAttributeValue},
                {"A struct type", nameof(DateTime)},
                {"A nullable", nameof(Nullable<int>)}
            };

            Write.ToConsole(examples);
        }
    }
}