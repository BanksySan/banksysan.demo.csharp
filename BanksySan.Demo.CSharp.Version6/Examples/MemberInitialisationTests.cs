﻿namespace BanksySan.Demo.CSharp.Version6.Examples
{
    using System;
    using NUnit.Framework;

    [TestFixture]
    public class MemberInitialisationTests
    {
        [Test]
        public void Example()
        {
            var memberInitialisation = new MemberInitialisation();

            Console.WriteLine(memberInitialisation.ReadOnlyProperty);
            Console.WriteLine(memberInitialisation.ReadWriteProperty);

            memberInitialisation.ReadWriteProperty = "I've changed the property.";

            Console.WriteLine(memberInitialisation.ReadWriteProperty);

            Console.WriteLine(StaticMemberInitialisation.ReadOnlyProperty);
            Console.WriteLine(StaticMemberInitialisation.ReadWriteProperty);
            StaticMemberInitialisation.ReadWriteProperty = "I've changed the static property.";
            Console.WriteLine(StaticMemberInitialisation.ReadWriteProperty);
        }
    }
}