﻿namespace BanksySan.Demo.CSharp.Version6.Examples
{
    using System;
    using System.Threading.Tasks;
    using NUnit.Framework;

    [TestFixture]
    public class AwaitCatchFinallyBlockTests
    {
        private Task DoSomething()
        {
            var task = new Task(() => { Console.WriteLine("Doing Something..."); });
            task.Start();
            return task;
        }

        [Test]
        public async Task Example()
        {
            try
            {
                throw new Exception("This is an exception.");
            }
            catch (Exception)
            {
                await DoSomething();
            }
            finally
            {
                await DoSomething();
            }
        }
    }
}