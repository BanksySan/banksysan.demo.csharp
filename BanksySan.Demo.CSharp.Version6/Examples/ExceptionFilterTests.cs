﻿namespace BanksySan.Demo.CSharp.Version6.Examples
{
    using System;
    using NUnit.Framework;

    [TestFixture]
    public class ExceptionFilterTests
    {
        private bool IsItParam2(ArgumentNullException e)
        {
            return e.ParamName == "param2";
        }

        private bool IsItParam1(ArgumentNullException e)
        {
            return e.ParamName == "param1";
        }

        [Test]
        public void ExampleWithAbstractFilter()
        {
            var exceptionalFiltered = new ExceptionalFiltered();
            var exceptionalUnfiltered = new ExceptionalUnfiltered();

            exceptionalFiltered.ThrowException();
            exceptionalUnfiltered.ThrowException();
        }

        [Test]
        public void ExampleWithPredicate()
        {
            try
            {
                throw new ArgumentNullException("param2");
            }
            catch (ArgumentNullException e) when (IsItParam1(e))
            {
                Console.WriteLine("Param1 is null");
            }
            catch (ArgumentNullException e) when (IsItParam2(e))
            {
                Console.WriteLine("Param2 is null");
            }
            catch (ArgumentNullException)
            {
                Console.WriteLine("A different param is null");
            }
        }
    }
}