namespace BanksySan.Demo.CSharp.Version6.Examples
{
    internal class MemberInitialisation
    {
        public string ReadOnlyProperty { get; } = "This is a read-only property.";
        public string ReadWriteProperty { get; set; } = "This is a read and write property.";
    }
}