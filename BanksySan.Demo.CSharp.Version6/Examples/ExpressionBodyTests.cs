﻿namespace BanksySan.Demo.CSharp.Version6.Examples
{
    using System;
    using NUnit.Framework;

    [TestFixture]
    public class ExpressionBodyTests
    {
        public long OldStyleMethod(DateTime dateTime, long i)
        {
            return dateTime.Ticks + i;
        }

        public long ExpressionBodyMethod(DateTime dateTime, long i)
        {
            return dateTime.Ticks + i;
        }

        public long OldStyleProperty => DateTime.Now.Ticks;

        public long ExpressionBodyProperty => DateTime.Now.Ticks;

        [Test]
        public void Example()
        {
            var oldStyleProperty = OldStyleProperty;
            var expressionBodyProperty = ExpressionBodyProperty;

            const long INTERVAL = 1000;
            var now = DateTime.Now;

            var oldStyleMethod = OldStyleMethod(now, INTERVAL);
            var expressionBodyMethod = ExpressionBodyMethod(now, INTERVAL);
            Console.WriteLine("Old style property: {0}", oldStyleProperty);
            Console.WriteLine("Old style method: {0}", oldStyleMethod);
            Console.WriteLine("Expression style property: {0}", expressionBodyProperty);
            Console.WriteLine("Expression style method: {0}", expressionBodyMethod);
        }
    }
}