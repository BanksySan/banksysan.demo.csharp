﻿namespace BanksySan.Demo.CSharp.Version6.Examples
{
    using System;
    using System.Collections.Generic;
    using NUnit.Framework;

    [TestFixture]
    public class StringInterpolationTests
    {
        [Test]
        public void Examples()
        {
            const string FIRST_NAME = "David";
            const string LAST_NAME = "Banks";
            const string X = "X";
            var age = 21.25;
            var integer = int.MaxValue;
            const string CONSTANT_FORMAT_STRING = "#;(#);Nothing";
            var now = DateTime.Now;

            var examples = new Dictionary<string, string>
            {
                {"Vanilla", $"My name is {FIRST_NAME} {LAST_NAME}."},
                {
                    "Verbatum",
                    $@"You can find my banks details at ""C:\bank\{FIRST_NAME}-{LAST_NAME}"""
                },
                {
                    "Fixed decimal",
                    $"I can format {age}, {age: 0.0}, {age: 0.00}, {age: 0.000}"
                },
                {
                    "Fixed percentage",
                    $"{age}: {age: 0.0%}, {age: 0.0%}, {age: 0.000%}"
                },
                {
                    "Optional decimal",
                    $"{age}: {age: 0.0}, {age: 0.00}, {age: 0.000}"
                },
                {
                    "Optional percentage",
                    $"{age}, {age: 0.#%}, {age: 0.#%}, {age: 0.###%}"
                },
                {
                    "Permille", $"{age}: {age:0.0‰}"
                },
                {
                    "Padding left and right",
                    $"|{X}|: |{X,-5}|, |{X,5}|"
                },
                {
                    "Padding numbers doesn't work!",
                    $"|{age}|: |{age,-5}|, |{age,5}|"
                },
                {
                    "Convert hex (lowercase)",
                    $"{integer}: {integer:x}, {integer:x10}"
                },
                {
                    "Convert hex (Uppercase)",
                    $"{integer}: {integer:X}, {integer:X10}"
                },
                {
                    "Commas",
                    $"{integer:N}"
                },
                {
                    "Scientific notation",
                    $"{integer}: {integer:e2}, {integer:E2}, {integer:e-2}."
                },
                {
                    "Dates",
                    $"{now:yyyy-MM-dd}, {now:u}, {now:HH} hours, {now:sss} milliseconds"
                },
                {
                    "Custom +ve, -ve & zero",
                    $"{1:#;(#);Nothing}, {-1:#;(#);Nothing}, {0:#;(#);Nothing}"
                },
                {
                    "Can't use format variable",
                    string.Format(
                        "I'm using string.Format here!  0 = {0:" + CONSTANT_FORMAT_STRING +
                        "}, 1 = {1:" + CONSTANT_FORMAT_STRING + "},  0 = {2:" +
                        CONSTANT_FORMAT_STRING + "}", 1, -1, 0)
                }
            };
            var s = $@"\";
            Write.ToConsole(examples);
        }
    }
}