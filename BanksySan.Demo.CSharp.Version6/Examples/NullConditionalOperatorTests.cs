﻿namespace BanksySan.Demo.CSharp.Version6.Examples
{
    using System;
    using NUnit.Framework;

    [TestFixture]
    public class NullConditionalOperatorTests
    {
        [Test]
        public void Examples()
        {
            var foo = new Foo();
            Action action = () => { };

            var bar1 = foo?.Calculate<int>();
            var bar2 = foo?[12];
            action?.Invoke();
        }
    }
}