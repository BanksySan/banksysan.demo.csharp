[TOC]

# New Features in C# 7

## Introduction

C# 7 was released earlier this year and have a bunch of other features.  Some features are, according to the Microsoft blogs, going to be something that they will be expanding on in the future (that being pattern matching).  

Two features require you to reference new assemblies:

* `ValueTuples` requires `System.ValueTuple`
* `ValueTask` requires `System.Threading.Tasks.Extensions`

## List of new Features

### Expression level
* Underscores as delimiters in integer numeric types.
* Pattern matching.
* Implicit `out` parameter declaration.
* `ValueTuple` type.

### Statement level
* Local functions.
* Throw expressions.
* Return by `ref`.

### Member level
* Deconstructors.
* `ValueTask` type.


# New Features in C# 6

## Introduction

C# 6 was released in July 2015.  It a _feature version_, not bringing many huge changes (e.g generics in C# 2), but it does bring some nice syntactic sugar to make C# easier on your eyes and on your fingers.

For details, have a look at the [wiki](https://bitbucket.org/BanksySan/banksysan.demo.csharp/wiki/Home).

## List of new Features

### Expression-level

* nameof(thing)
* String Interpolation
* Index Initializers
* Null conditional operator
* Extension Add<T>(T) for collection initialisers.

### Statement-level
* Exception filters
* await in catch and finally blocks

### Member-level
* Auto-property initialisers
* Static imports
* Expression bodies